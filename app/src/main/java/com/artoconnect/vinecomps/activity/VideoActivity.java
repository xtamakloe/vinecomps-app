package com.artoconnect.vinecomps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.adapter.VideoListAdapter;

public class VideoActivity extends AppCompatActivity {

    FrameLayout.LayoutParams COVER_SCREEN_GRAVITY_CENTER = new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
    private WebView mWebView;
    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private WebChromeClient mWebChromeClient;
    private View mCustomView;
    private Toolbar mToolbar;
    private String mCurrentUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        mContentView = (RelativeLayout) findViewById(R.id.relativelayout);
        mWebView = (WebView) findViewById(R.id.webView);
        mCustomViewContainer = (FrameLayout) findViewById(R.id.fullscreen_custom_content);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_arrow_left_white_24dp);
        mToolbar.setTitle(getResources().getString(R.string.app_name));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mWebChromeClient = new WebChromeClient() {

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                // if a view already exists then immediately terminate the new one
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }

                // Add the custom view to its container.
                mCustomViewContainer.addView(view, COVER_SCREEN_GRAVITY_CENTER);
                mCustomView = view;
                mCustomViewCallback = callback;

                // hide main browser view
                mContentView.setVisibility(View.GONE);

                // Finally show the custom view container.
                mCustomViewContainer.setVisibility(View.VISIBLE);
                mCustomViewContainer.bringToFront();
            }

            @Override
            public void onHideCustomView() {
                if (mCustomView == null)
                    return;

                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);
                // Remove the custom view from its container.
                mCustomViewContainer.removeView(mCustomView);
                mCustomView = null;
                mCustomViewContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();

                // Show the content view.
                mContentView.setVisibility(View.VISIBLE);
            }
        };

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        // get url from intent arguments
        mWebView.loadUrl(getIntent().getStringExtra(VideoListAdapter.EXTRA_URL));
        mWebView.setWebViewClient(new CustomWebViewClient());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCustomView != null) {
            if (mCustomViewCallback != null)
                mCustomViewCallback.onCustomViewHidden();
            mCustomView = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mCustomView != null) {
            mWebChromeClient.onHideCustomView();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url) {
            webview.setWebChromeClient(mWebChromeClient);
            webview.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            mCurrentUrl = url;

            // TODO: create video here using background task to fetch details from api
            //          save video if user wants. or just wait till user clicks on
            //          add to favourites or watch later and do this
        }
    }

}
