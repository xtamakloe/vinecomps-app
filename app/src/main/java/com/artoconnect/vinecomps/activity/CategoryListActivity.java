package com.artoconnect.vinecomps.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.adapter.CategoryListAdapter;
import com.artoconnect.vinecomps.helper.APIService;
import com.artoconnect.vinecomps.helper.SimpleDividerItemDecoration;
import com.artoconnect.vinecomps.model.Category;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;

public class CategoryListActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private List<Category> mCategories;
    private CategoryListAdapter mAdapter;
    private RelativeLayout mErrorView;
    private ProgressBar mProgressbar;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_arrow_left_grey600_24dp);
        mToolbar.setTitle(getResources().getString(R.string.action_categories));
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.material_drawer_secondary_text));
        mToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.md_white_1000));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Category list
        mRecyclerView = (RecyclerView) findViewById(R.id.rvCategories);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        // Progress bar
        mProgressbar = (ProgressBar) findViewById(R.id.pbCategoryList);

        // Set up and hide empty view
        mErrorView = (RelativeLayout) findViewById(R.id.rlErrorView);
        mErrorView.setVisibility(View.GONE);

        loadCategories();
    }

    private void loadCategories() {
        // Show progress bar
        mRecyclerView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.GONE);
        mProgressbar.setVisibility(View.VISIBLE);


        mCategories = Category.getCategories();
        if (mCategories.size() > 0) {
            mAdapter = new CategoryListAdapter(mCategories);
            mRecyclerView.setAdapter(mAdapter);

            mProgressbar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            // Get latest categories and update view
            APIService.getClient().getCategories().enqueue(new Callback<List<Category>>() {
                @Override
                public void onResponse(Response<List<Category>> response) {
                    mCategories = response.body();
                    mAdapter = new CategoryListAdapter(mCategories);
                    mRecyclerView.setAdapter(mAdapter);

                    // Persist
                    for (Category category: mCategories) {
                        category.save();
                    }

                    mProgressbar.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Throwable t) {
                    mProgressbar.setVisibility(View.GONE);
                    mErrorView.setVisibility(View.VISIBLE);
                    // Retry button
                    findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadCategories();
                        }
                    });
                }
            });
        }
    }
}
