package com.artoconnect.vinecomps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.fragment.HomeFragment;


public class MainActivity extends BaseActivity {

    private Toolbar mToolbar;
    //    private Drawer mDrawer;
    private OnFilterChangedListener onFilterChangedListener;
    private Fragment mFragment;

    public void setOnFilterChangedListener(OnFilterChangedListener onFilterChangedListener) {
        this.onFilterChangedListener = onFilterChangedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setTitle(getResources().getString(R.string.title_browse));
        setSupportActionBar(mToolbar);

        /*
        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_all)
                                .withIdentifier(Category.ALL.id)
                                .withIcon(GoogleMaterial.Icon.gmd_video_library),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_categories)
                                .withIdentifier(Category.CATEGORIES.id)
                                .withIcon(GoogleMaterial.Icon.gmd_style),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_settings)
                                .withIdentifier(Category.SETTINGS.id)
                                .withIcon(GoogleMaterial.Icon.gmd_settings),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_rate_app)
                                .withIdentifier(Category.RATE_APP.id)
                                .withIcon(GoogleMaterial.Icon.gmd_thumb_up),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_share_app)
                                .withIdentifier(Category.SHARE_APP.id)
                                .withIcon(GoogleMaterial.Icon.gmd_share),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_support)
                                .withIdentifier(Category.CONTACT_SUPPORT.id)
                                .withIcon(GoogleMaterial.Icon.gmd_help)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        if (iDrawerItem != null) {
                            if (iDrawerItem instanceof Nameable) {
                                mToolbar.setTitle(((Nameable) iDrawerItem).getNameRes());
                            }
                            if (onFilterChangedListener != null) {
                                onFilterChangedListener.onFilterChanged(iDrawerItem.getIdentifier());
                            }
                            openSelectedItem(iDrawerItem.getIdentifier());
                        }
                        return false;
                    }
                })
                .build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        mDrawer.getListView().setVerticalScrollBarEnabled(false);
        */

        // Load home fragment initially
        mFragment = HomeFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mFragment, "mFragment")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;

            case R.id.action_categories:
                Intent intent = new Intent(this, CategoryListActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* for navigation drawer
    public void openSelectedItem(int selectedItemId) {
        if (selectedItemId == Category.ALL.id) {
            mFragment = HomeFragment.newInstance();
        } else if (selectedItemId == Category.CATEGORIES.id) {
            mFragment = CategoriesFragment.newInstance("");
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mFragment, "mFragment")
                .commit();
    }*/

    public enum Category {
        ALL(1000),
        CATEGORIES(1001),
        SETTINGS(1),
        RATE_APP(2),
        SHARE_APP(3),
        CONTACT_SUPPORT(4);

        public final int id;

        private Category(int id) {
            this.id = id;
        }
    }

    public interface OnFilterChangedListener {
        public void onFilterChanged(int filter);
    }
}
