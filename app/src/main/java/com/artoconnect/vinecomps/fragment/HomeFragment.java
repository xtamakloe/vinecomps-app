package com.artoconnect.vinecomps.fragment;


import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.adapter.HomeFragmentPagerAdapter;
import com.mikepenz.google_material_typeface_library.GoogleMaterial.Icon;
import com.mikepenz.iconics.IconicsDrawable;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private Integer[] mTabTitles = {
            R.string.title_browse,
            R.string.title_favourites,
            R.string.title_watch_later
    };

    private Icon[] mTabIcons = {
            Icon.gmd_video_library,
            Icon.gmd_favorite,
            Icon.gmd_alarm
    };


    private TabLayout mTabLayout;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        final HomeFragmentPagerAdapter mPagerAdapter =
                new HomeFragmentPagerAdapter(getActivity(), getActivity().getSupportFragmentManager());
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.vpPager);
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        viewPager.setAdapter(mPagerAdapter);
        viewPager.setCurrentItem(0); // added
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);

                mTabLayout.getTabAt(position).select(); // Set current tab
                setHighlight(mTabLayout.getTabAt(position)); // Highlight current tab

                // Set title based on current tab
                String title = getActivity().getResources().getString(mTabTitles[position]);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        for (Icon icon : mTabIcons) {
            mTabLayout.addTab(mTabLayout.newTab().setIcon(
                    new IconicsDrawable(getContext())
                            .icon(icon)
                            .color(R.color.primary_light)
                            .sizeDp(24)));
        }
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                // Highlight current tab
                setHighlight(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        setHighlight(mTabLayout.getTabAt(0));
        return view;
    }

    void setHighlight(TabLayout.Tab tab) {
        for (int i = 0; i < 3; i++) {
            mTabLayout.getTabAt(i).getIcon().clearColorFilter();
        }
        tab.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }
}
