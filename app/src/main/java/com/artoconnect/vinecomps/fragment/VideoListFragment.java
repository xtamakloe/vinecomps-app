package com.artoconnect.vinecomps.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.adapter.VideoListAdapter;
import com.artoconnect.vinecomps.event.SelectCategoryEvent;
import com.artoconnect.vinecomps.helper.SimpleDividerItemDecoration;
import com.artoconnect.vinecomps.helper.Util;
import com.artoconnect.vinecomps.helper.YoutubeConnector;
import com.artoconnect.vinecomps.model.Video;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link Fragment} subclass.
 */
public class VideoListFragment extends Fragment {

    private static final String TAG = "VideoListFragment";
    private static final String ARG_CATEGORY = "category";

    private RecyclerView mRecyclerView;
    private VideoListAdapter mAdapter;
    private Handler mHandler;
    private List<Video> mSearchResults;
    private ProgressBar mProgressbar;
    private RelativeLayout mErrorNotice;
    private String mSearchTerm;

    public VideoListFragment() {
        // Required empty public constructor
    }

    public static VideoListFragment newInstance(String category) {
        VideoListFragment fragment = new VideoListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_list, container, false);
        rootView.setTag(TAG);

        mProgressbar = (ProgressBar) rootView.findViewById(R.id.pbVideoList);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvVideos);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        mErrorNotice = (RelativeLayout) rootView.findViewById(R.id.rlErrorNotice);
        mErrorNotice.setVisibility(View.GONE);

        // Hide empty notice indefinitely
        rootView.findViewById(R.id.rlEmptyNotice).setVisibility(View.GONE);

        // Retry button
        rootView.findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadVideos(mSearchTerm);
            }
        });

        // Initialise data
        loadVideos(mSearchTerm);

        return rootView;
    }

    private void loadVideos(String keywords) {
        mRecyclerView.setVisibility(View.GONE);
        mErrorNotice.setVisibility(View.GONE);
        mProgressbar.setVisibility(View.VISIBLE);

        if (Util.isConnected(getActivity())) {
            mHandler = new Handler();

            if (keywords == null) {
                mSearchTerm = getArguments().getString(ARG_CATEGORY);
            } else {
                mSearchTerm = keywords;
            }

            searchOnYoutube(mSearchTerm + " compilations 2015");
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();

            mProgressbar.setVisibility(View.GONE);
            mErrorNotice.setVisibility(View.VISIBLE);
        }
    }

    private void searchOnYoutube(final String keywords) {
        new Thread() {
            public void run() {
                YoutubeConnector yc = new YoutubeConnector(getActivity());
                mSearchResults = yc.search(keywords);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressbar.setVisibility(View.GONE);

                        if (mSearchResults == null) {
                            Snackbar.make(getActivity().findViewById(android.R.id.content),
                                    R.string.network_error, Snackbar.LENGTH_LONG).show();

                            mErrorNotice.setVisibility(View.VISIBLE);
                        } else {
                            mAdapter = new VideoListAdapter(mSearchResults, false);
                            mRecyclerView.setAdapter(mAdapter);

                            mRecyclerView.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }.start();
    }


    @Override
    public void onStart() {
        super.onStart();
        // Register class only once in app lifetime
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(SelectCategoryEvent event) {
        loadVideos(event.mCategory.getKeywords());
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                "Loading " + event.mCategory.getTitle() + " Compilations...",
                Snackbar.LENGTH_LONG).show();
    }
}
