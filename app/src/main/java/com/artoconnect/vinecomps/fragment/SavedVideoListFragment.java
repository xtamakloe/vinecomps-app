package com.artoconnect.vinecomps.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.adapter.VideoListAdapter;
import com.artoconnect.vinecomps.event.SaveVideoEvent;
import com.artoconnect.vinecomps.helper.SimpleDividerItemDecoration;
import com.artoconnect.vinecomps.model.Video;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link Fragment} subclass.
 */
public class SavedVideoListFragment extends Fragment {

    public static final String FAVOURITES = "favourites";
    public static final String WATCH_LATER = "watch_later";
    private static final String ARG_LIST_TYPE = "com.artoconnect.vinecomps.fragment.LIST_TYPE";
    private static final String TAG = "SavedVideoListFragment";
    private RecyclerView mRecyclerView;
    private RelativeLayout mEmptyNotice;
    private VideoListAdapter mAdapter;
    private String mListType;
    private List<Video> mVideos;


    public SavedVideoListFragment() {
        // Required empty public constructor
    }

    public static SavedVideoListFragment newInstance(String type) {
        SavedVideoListFragment fragment = new SavedVideoListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LIST_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_list, container, false);
        rootView.setTag(TAG);
        // Hide progress bar and empty results indefinitely for now
        rootView.findViewById(R.id.pbVideoList).setVisibility(View.GONE);
        rootView.findViewById(R.id.rlErrorNotice).setVisibility(View.GONE);

        // Set up recycler view
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvVideos);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        // Set up and hide notices
        mEmptyNotice = (RelativeLayout) rootView.findViewById(R.id.rlEmptyNotice);
        mEmptyNotice.setVisibility(View.GONE);

        // Initialise data
        loadVideos();

        return rootView;
    }

    private void loadVideos() {
        mListType = getArguments().getString(ARG_LIST_TYPE);

        if (mListType == FAVOURITES) {
            mVideos = Video.getFavourites();
        } else if (mListType == WATCH_LATER) {
            mVideos = Video.getWatchList();
        }

        if (mVideos != null && mVideos.size() > 0) {
            // show video list
            mAdapter = new VideoListAdapter(mVideos, true);
            mRecyclerView.setAdapter(mAdapter);

            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyNotice.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyNotice.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(SaveVideoEvent event) {
        loadVideos();
    }
}
