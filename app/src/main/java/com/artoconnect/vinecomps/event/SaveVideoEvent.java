package com.artoconnect.vinecomps.event;

import com.artoconnect.vinecomps.model.Video;

/**
 * Created by Christian Tamakloe on 07/09/15.
 */
public class SaveVideoEvent {
    public final Video mVideo;

    public SaveVideoEvent(Video video) {
        this.mVideo = video;
    }
}