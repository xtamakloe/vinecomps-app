package com.artoconnect.vinecomps.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Christian Tamakloe on 07/09/15.
 */
@Table(name = "categories")
public class Category extends Model {

    @Column(name = "title")
    private String title;

    @Column(name = "keywords")
    private String keywords;

    public Category() {
    }

    public Category(String name, String keywords) {
        this.title = name;
        this.keywords = keywords;
    }

    public static List<Category> getCategories() {
        return new Select().from(Category.class)
                .orderBy("title ASC")
                .execute();
    }

    public static void clearCategories() {
        new Delete().from(Category.class).execute();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
}
