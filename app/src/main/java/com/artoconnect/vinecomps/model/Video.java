package com.artoconnect.vinecomps.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.artoconnect.vinecomps.helper.Util;

import java.util.List;

/**
 * Created by Christian Tamakloe on 17/07/15.
 */
@Table(name = "videos")
public class Video extends Model {

    @Column(name="uid", index=true)
    private String uid;

    @Column(name="title")
    private String title;

    @Column(name="url")
    private String url;

    @Column(name="thumbnail_url")
    private String thumbnailUrl;

    @Column(name="favourite")
    private boolean isFavourite;

    @Column(name="watch_later")
    private boolean watchLater;

    public Video() {

    }

    public Video(String title, String thumbnailUrl, String uid, String url, boolean isFavourite, boolean watchLater) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.uid = uid;
        this.url = url;
        this.isFavourite = isFavourite;
        this.watchLater = watchLater;
    }

    public static void watchLater(Video video) {
        Video v = new Select()
                .from(Video.class)
                .where("uid = ?", video.getUid())
                .executeSingle();
        if (v == null) {
            v = new Video(video.getTitle(),
                    video.getThumbnailUrl(),
                    video.getUid(),
                    video.getUrl(),
                    false,
                    true);
        } else {
            v.setWatchLater(true);
        }
        v.save();
    }

    public static void addToFavourites(Video video) {
        Video v = new Select()
                .from(Video.class)
                .where("uid = ?", video.getUid())
                .executeSingle();
        if (v == null) {
            v = new Video(video.getTitle(),
                    video.getThumbnailUrl(),
                    video.getUid(),
                    video.getUrl(),
                    true,
                    false);
        } else {
            v.setIsFavourite(true);
        }
        v.save();
    }

    public static List<Video> getFavourites() {
        return new Select().from(Video.class)
                .orderBy("title ASC")
                .where("favourite = 1")
                .execute();
    }

    public static List<Video> getWatchList() {
        return new Select().from(Video.class)
                .orderBy("title ASC")
                .where("watch_later = 1")
                .execute();
    }

    public static void remove(Video video) {
        new Delete().from(Video.class)
                .where("uid = ?", video.getUid())
                .execute();
    }

    public void setWatchLater(boolean watchLater) {
        this.watchLater = watchLater;
    }

    public void setIsFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return Util.titleize(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
