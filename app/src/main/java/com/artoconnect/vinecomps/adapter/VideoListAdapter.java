package com.artoconnect.vinecomps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.activity.VideoActivity;
import com.artoconnect.vinecomps.event.SaveVideoEvent;
import com.artoconnect.vinecomps.model.Video;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Christian Tamakloe on 17/07/15.
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    public static final String EXTRA_URL = "com.artoconnect.vinecomps.adapter.EXTRA_URL";
    private boolean mSavedVideos;
    private List<Video> mVideos;


    public VideoListAdapter(List<Video> videos, boolean saved) {
        this.mSavedVideos = saved;

        if (videos == null) {
            this.mVideos = Collections.<Video>emptyList();
        } else {
            this.mVideos = videos;
        }
    }

    @Override
    public VideoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_video, parent, false);

        // set view's size, margins, paddings and layout params
        // ...

        // Return viewholder
        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(VideoListAdapter.ViewHolder holder, int position) {
        Video video = mVideos.get(position);

        holder.mTitle.setText(video.getTitle());
        Picasso.with(holder.mTitle.getContext())
                .load(video.getThumbnailUrl())
                .resize(128, 80)
                .centerCrop()
//                .placeholder(R.drawable.user_placeholder)
//                .error(R.drawable.user_placeholder_error)
                .into(holder.mThumbnail);
        holder.mCurrentVideo = video; //mVideos.get(position);
    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {
        public TextView mTitle;
        public ImageView mThumbnail;
        public ImageView mContextMenuIcon;
        public Video mCurrentVideo;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            mThumbnail = (ImageView) itemView.findViewById(R.id.ivThumbnail);
            mContextMenuIcon = (ImageView) itemView.findViewById(R.id.context_menu);
            mContextMenuIcon.setOnClickListener(this);
//            mThumbnail.setOnClickListener(this);
//            mTitle.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == mContextMenuIcon) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                //   inflate different menu based on saved flag
                if (mSavedVideos) {
                    popupMenu.inflate(R.menu.menu_saved_video_item);
                } else {
                    popupMenu.inflate(R.menu.menu_video_item);
                }
                popupMenu.setOnMenuItemClickListener(this);
                popupMenu.show();
            } else {
                Intent intent = new Intent(view.getContext(), VideoActivity.class);
                intent.putExtra(EXTRA_URL, mCurrentVideo.getUrl());
                view.getContext().startActivity(intent);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            int message = 0;

            switch (menuItem.getItemId()) {
                case R.id.action_favourite:
                    Video.addToFavourites(mCurrentVideo);
                    message = R.string.notification_added_favourites;
                    break;

                case R.id.action_watch_later:
                    Video.watchLater(mCurrentVideo);
                    message = R.string.notification_added_watch_list;
                    break;

                case R.id.action_remove:
                    Video.remove(mCurrentVideo);
                    message = R.string.notification_video_removed;
                    break;

                case R.id.action_share:
                    message = 0;
                    break;

                default:
                    break;
            }
            if (message != 0) {
                Snackbar.make(mContextMenuIcon.getRootView().findViewById(android.R.id.content),
                        message,
                        Snackbar.LENGTH_LONG).show();
                EventBus.getDefault().post(new SaveVideoEvent(mCurrentVideo));
            }

            return true;
        }
    }
}
