package com.artoconnect.vinecomps.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.event.SelectCategoryEvent;
import com.artoconnect.vinecomps.model.Category;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Christian Tamakloe on 08/09/15.
 */
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private final List<Category> mCategories;


    public CategoryListAdapter(List<Category> categories) {
        this.mCategories = categories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_category, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Category category = mCategories.get(position);
        holder.mTitle.setText(category.getTitle());
        holder.mCategory = category;
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private final TextView mTitle;
        private Category mCategory;

        public ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new SelectCategoryEvent(mCategory));
            ((AppCompatActivity) view.getContext()).finish();
        }
    }

}
