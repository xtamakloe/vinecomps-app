package com.artoconnect.vinecomps.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.fragment.SavedVideoListFragment;
import com.artoconnect.vinecomps.fragment.VideoListFragment;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

/**
 * Created by Christian Tamakloe on 02/09/15.
 */
public class HomeFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private static final int TAB_COUNT = 3;
    private final Context mContext;
    Drawable mIcon;


    public HomeFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return VideoListFragment.newInstance("funny video");

            case 1:
                return SavedVideoListFragment.newInstance(SavedVideoListFragment.FAVOURITES);

            case 2:
                return SavedVideoListFragment.newInstance(SavedVideoListFragment.WATCH_LATER);

            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

}
