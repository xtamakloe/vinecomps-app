package com.artoconnect.vinecomps.helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.artoconnect.vinecomps.R;
import com.artoconnect.vinecomps.model.Video;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian Tamakloe on 18/07/15.
 */
public class YoutubeConnector {
    public static final String KEY = "AIzaSyA1pTTk5L8KIT9wkscg2fcUlTELZgppuuM";
    private YouTube mYoutube;
    private YouTube.Search.List mQuery;

    public YoutubeConnector(Context context) {
        mYoutube = new YouTube.Builder(new NetHttpTransport(),
                new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest hr) throws IOException {
            }
        }).setApplicationName(context.getString(R.string.app_name)).build();

        try {
            mQuery = mYoutube.search().list("id,snippet");
            mQuery.setKey(KEY);
            mQuery.setType("video");
            mQuery.setMaxResults(40L);
            mQuery.setFields("items(id/videoId,snippet/title,snippet/thumbnails/default/url)");
        } catch (IOException e) {
            Log.d("YC", "Could not initialize: " + e);
        }
    }

    public List<Video> search(String keywords) {
        List<Video> videos = new ArrayList<>();
        mQuery.setQ(keywords);
        try {
            SearchListResponse response = mQuery.execute();
            List<SearchResult> results = response.getItems();
            for (SearchResult result : results) {
                Video video = new Video();
                video.setTitle(result.getSnippet().getTitle());
                video.setThumbnailUrl(result.getSnippet().getThumbnails().getDefault().getUrl());
                video.setUid(result.getId().getVideoId());
                video.setUrl("https://www.youtube.com/watch?v=" + result.getId().getVideoId());
                videos.add(video);
            }
            return videos;
        } catch (IOException e) {
            Log.d("YC", "Could not search: " + e);
//            return videos;
            return null;
        }
    }
}
