package com.artoconnect.vinecomps.helper;

import com.artoconnect.vinecomps.model.Category;

import java.util.List;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;

/**
 * Created by Christian Tamakloe on 07/09/15.
 */
public class APIService {
    public static final String API_URL = "https://blooming-depths-4297.herokuapp.com/api/v1/";
    private static ApiInterface sApiInterface;


    public static ApiInterface getClient() {
        if (sApiInterface == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            sApiInterface = retrofit.create(ApiInterface.class);
        }
        return sApiInterface;
    }

    public interface ApiInterface {
        @GET("categories")
        Call<List<Category>> getCategories();
    }
}
